// Import
import { ApiPromise, WsProvider } from '@polkadot/api';
import { BlockHash } from '@polkadot/types/interfaces';
import * as fsPromises from 'fs/promises';
import fsExists from 'fs.promises.exists';
import { exit } from 'process';

const decimalPlaces = 9;
const TREASURY_ACCOUNT = "6XmmXY7zLRirfFQivNnn6LNyRP1aMvtzyr4gATsfbdFh2QqF";

const wsProvider = new WsProvider('ws://127.0.0.1:63007');
const api = await ApiPromise.create({ provider: wsProvider });
import { createObjectCsvWriter } from 'csv-writer';

await api.isReady;

let startBlockNumber = 3475240;

const currentHeader = await api.rpc.chain.getHeader();
const currentBlockNumber = currentHeader.number.toNumber();
console.log(`Current block height: ${currentBlockNumber}`);

let allTransfers : Object[] = [];

const fileExists = await fsExists("tips.json");

if (fileExists) {
    const data = await fsPromises.readFile("tips.json", {encoding: 'utf-8'});
    const obj = JSON.parse(data); 
    console.log(`Found existing tips.json Loaded ${obj['tips'].length} records. Last block height: ${obj['lastBlock']}`);
    startBlockNumber = obj['lastBlock'] + 1;
    allTransfers = obj['tips'];
}

for (let i = startBlockNumber; i <= currentBlockNumber; i++) {
    const blockHash = await api.rpc.chain.getBlockHash(i);
    const transfers = await parseBlock(blockHash);
    if (transfers.length > 0) {
        allTransfers = allTransfers.concat(transfers);
        console.log(transfers);
    }
}

const records = {
    "lastBlock" : currentBlockNumber,
    "tips" : allTransfers
}

await fsPromises.writeFile("tips.json", JSON.stringify(records, null, 4),  {encoding: 'utf-8'});
const date = new Date();
const csvWriter = createObjectCsvWriter({
    path: `tips-${date.getUTCFullYear()}-${date.getUTCMonth()+1}-${date.getUTCDate()}.csv`,
    header: [
        {id: 'blockNumber', title: 'blockNumber'},
        {id: 'source', title: 'source'},
        {id: 'dest', title: 'dest'},
        {id: 'amount', title: 'amount'}
    ]
});

await csvWriter.writeRecords(records['tips']);

exit(0);

async function parseBlock(hash: BlockHash): Promise<Array<Object>> {
    let treasuryTransfers : Object[] = [];
    const signedBlock = await api.rpc.chain.getBlock(hash);
    const blockNumber = signedBlock.block.header.number.toNumber();
    console.log(`Checking block ${blockNumber} for tip close extrinsics`);
    let index = 0;
    for (const ext of signedBlock.block.extrinsics)
    {
        if (ext.method.section == "tips" && ext.method.method == "closeTip") {
            const apiAt = await api.at(signedBlock.block.header.hash);
            const allRecords = await apiAt.query.system.events();
            let extTransfers : Object[] = [];
            const events = allRecords
            .filter(({ phase }) =>
                phase.isApplyExtrinsic &&
                phase.asApplyExtrinsic.eq(index)
            );

            events.forEach(({event}) => {
                if (event.section == "balances" && event.method == "Transfer") {
                    const source = event.data[0].toString();
                    const dest = event.data[1].toString();
                    const amount = parseInt(event.data[2].toString()) / 10 ** decimalPlaces;
                    if (source == TREASURY_ACCOUNT) {
                        extTransfers.push({blockNumber, source, dest, amount});
                    }
                }

                if (apiAt.events.system.ExtrinsicSuccess.is(event)) {
                    treasuryTransfers = treasuryTransfers.concat(extTransfers);
                }
            })
        }
        index++
    }
    return treasuryTransfers;
}